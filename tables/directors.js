const movieData = require('./movies.json');
const { Pool } = require('pg');

const pool= new Pool({
    user:"postgres",
    password:"postgreskapassword",
    host:"localhost",
    port:5432,
    database:"movierentaldatabase"
})
// console.log(movieData)


const directors = async (movieData) => {
 pool.query(`create table if not exists  directors (id serial primary key, name varchar(40) unique)`);  
   const x= movieData.reduce((acc, res) => {
    if (acc[res.Director] == undefined) {
      acc[res.Director] = 1;    
      pool.query(`insert into directors (name) values ($1)`, [
          res.Director
        ])
        
        .catch(err => err);
    }
    return acc;
  }, {});
  console.log("table directors created")
  };

function deleteTable(){
  pool.query(`DROP TABLE IF EXISTS directors`)
  console.log("directors table deleted")
}

directors(movieData)
// deleteTable()
