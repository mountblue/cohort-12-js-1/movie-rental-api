const express = require("express")
const bodyParser = require('body-parser')
const app=express()
const port=3000 
const db = require('./queries.js')


app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.get('/api/directors', db.getUsers)
app.get('/api/directors/:directorId', db.getUserById)
app.post('/api/directors', db.createUser)
app.put('/api/directors/:directorId', db.updateUser)
app.delete('/api/directors/:directorId', db.deleteUser)

app.get("/",(req,res)=>{
    res.sendfile( "./index.html")
})

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})