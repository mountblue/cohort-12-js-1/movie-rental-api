const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  password: "postgreskapassword",
  host: "localhost",
  port: 5432,
  database: "movierentaldatabase"
});

const getUsers = (request, response) => {
  pool.query("SELECT * FROM directors ORDER BY id ASC", (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};

const getUserById = (request, response) => {
  const id = parseInt(request.params.directorId)
  pool.query(
    "SELECT * FROM directors WHERE id = $1",
    [id],
    (error, results) => {
      if (error) {
        throw error;
      }
      response.status(200).json(results.rows);
    }
  );
};

const createUser = (request, response) => {
  const name  = request.body.name;
  console.log(request.body.name)

  pool.query(
    "INSERT INTO directors (name) VALUES ($1) RETURNING id",
    [name],
    (error, results) => {
      if (error) {
        throw error;
      }
      response.status(201).send(`User added with ID: ${results.rows[0].id}`);
    }
  );
};

const updateUser = (request, response) => {
  const id = parseInt(request.params.directorId);
  const { name } = request.body;

  pool.query(
    "UPDATE directors SET name = $1 WHERE id = $2",
    [name, id],
    (error, results) => {
      if (error) {
        throw error;
      }
      response.status(200).send(`User modified with ID: ${id}`);
    }
  );
};

const deleteUser = (request, response) => {
  const id = request.params.directorId;

  pool.query("DELETE FROM directors WHERE id = $1", [id], (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).send(`User deleted with ID: ${id}`);
  });
};

// async function createTable() {
//   await directors(movieData);
// //   await movies(movieData);
// }
// createTable().then(y=>console.log(y))
module.exports = {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser
};
// });//   'https://cors-anywhere.herokuapp.com/' +
//'https://cors-anywhere.herokuapp.com/' +
// 'https://trello-attachments.s3.amazonaws.com/5e4a127f9e8d93222b3c507a/5e65d2faf068528e4302b917/x/5b29e90033d644379ecd35553d0f4d98/movies.json'
